<?php
/* 0 + 1 を１回目とし、繰り返し回数を引数にとるfibonacci数列*/
function fibonacci1($i){
    if ($i > 0){
        return fibonacci1($i - 1) + fibonacci1($i - 2);
    } elseif ($i === 0){
        return 1;
    } else{
        return 0;
    }
}

echo fibonacci1(20);




