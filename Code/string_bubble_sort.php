<?php

function make_rand_str ($number, $length){  
    $str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
    $r_str = array();
    for ($i = 0; $i < $number; $i ++){
        $r_str[$i] = null;
        for ($j = 0; $j < $length; $j ++) {
            $r_str [$i].= $str[rand(0, count($str) - 1)];
        }
    }
    return $r_str;
}

$random_strings = make_rand_str (10, 10);
$random_strings[] = "aBcDe";  //テスト用
$random_strings[] = "AbCdE";  
$random_strings[] = "さしすせそ";  
$random_strings[] = "アいウえオ";  
$random_strings[] = "アIうeお";  
$random_strings[] = "あイうエお";  
$random_strings[] = "あiウEお";  


echo "配列を作成しました。\n";
print_r ($random_strings);
echo "\n";

for ($i = 0; $i < (count ($random_strings) - 1); $i ++) {   
    for ($j = 0; $j < (count ($random_strings) - ($i + 1)); $j ++) {
        if (mb_convert_kana (mb_strtoupper ($random_strings [$j]), "c") === mb_convert_kana (mb_strtoupper ($random_strings [$j + 1]), "c")) {
            if ($random_strings [$j] > $random_strings [$j + 1]) {   
                [$random_strings [$j], $random_strings [$j + 1]] = [$random_strings [$j + 1], $random_strings [$j]];
                }  
        } else {
            if (mb_convert_kana (mb_strtoupper ($random_strings [$j]), "c") > mb_convert_kana (mb_strtoupper ($random_strings [$j + 1]), "c")) {   
                [$random_strings [$j], $random_strings [$j + 1]] = [$random_strings [$j + 1], $random_strings [$j]];
                }    
        }
        
    }
}

echo "配列を昇順で並び変えました\n";
print_r($random_strings); 