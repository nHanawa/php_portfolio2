<?php

$animal = ["dog", "cat", "bird"];
$color = ["red" => "赤", "青", "yellow" => "黄"];
$fruits = ["apple", "yellow" => "lemon", "green" => "melon"];
$number = [1 => "1", 5 => "100", 10 =>"1000"];
$human = [
    ["田中", "woman"],
    ["佐藤", "man"],
    "guest" => ["林", "man"],
];



function my_array_merge($a, $c){
    foreach($c as $key => $value){
        if (is_string($key)){
            $a[$key] = $value;
        } else{
            $a [] = $value;
        }
    };
    return $a;
}
$merge = array();
$merge = my_array_merge($merge, $animal);
$merge = my_array_merge($merge, $color);
$merge = my_array_merge($merge, $fruits);
$merge = my_array_merge($merge, $number);
$merge = my_array_merge($merge, $human);
var_dump($merge);





    