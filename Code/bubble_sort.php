<?php
$number = array();

for($i = 0; $i < 10; $i ++) {
    $rand_number = rand (1, 100);
    $number[] = $rand_number;
}

echo "生成された配列\n";
print_r($number); 
echo "\n";

for($j = 0; $j < (count($number) - 1); $j ++) { 
    for($r = 0; $r < (count($number) - ($j + 1)); $r ++) {
        if($number[$r] > $number[$r + 1]) {   
            [$number[$r], $number[$r + 1]] = [$number[$r + 1], $number[$r]];
        }        
    }
}

echo "配列を昇順で並び変えました\n";
print_r($number); 